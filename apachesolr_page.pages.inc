<?php

/**
 * Displays a search page.
 *
 * @param $id
 *   The search page's ID.
 * @param $keys
 *   The keys to search for.
 */
function apachesolr_page_view($id, $keys = NULL) {
  $page = apachesolr_page_load((int) $id);
  if (!$page) {
    return MENU_NOT_FOUND;
  }

  $ret['form'] = drupal_get_form('apachesolr_page_search_form', $page, $keys);
  // @todo Link this to empty search actions.
//  if ($keys !== NULL) {
    $ret['results']['#theme'] = 'apachesolr_page_results';
    $ret['results']['#filters'] = array();
    $ret['results']['#results'] = $results = apachesolr_page_search_execute($page, $keys);
    $ret['results']['#view_mode'] = isset($page->options['view_mode']) ? $page->options['view_mode'] : 'apachesolr_page_result';
    $ret['results']['#keys'] = $keys;
    if (count($results) > $page->options['per_page']) {
      pager_default_initialize(count($results), $page->options['per_page']);
      $ret['pager']['#theme'] = 'pager';
      $ret['pager']['#quantity'] = (int) ceil(count($results) / $page->options['per_page']);
    }

    if (!empty($results['ignored'])) {
      drupal_set_message(t('The following search keys are too short or too common and were therefore ignored: "@list".', array('@list' => implode(t('", "'), $results['ignored']))), 'warning');
    }
    if (!empty($results['warnings'])) {
      foreach ($results['warnings'] as $warning) {
        drupal_set_message($warning, 'warning');
      }
    }
//  }

  return $ret;
}

/**
 * Executes a search.
 *
 * @param stdClass $page
 *   The page for which a search should be executed.
 * @param $keys
 *   The keywords to search for.
 *
 * @return array
 *   The search results as returned by SearchApiQueryInterface::execute().
 */
function apachesolr_page_search_execute(stdClass $page, $keys) {
  $filters = isset($page->filters) ? $page->filters : '';
  $solrsort = isset($_GET['solrsort']) ? $_GET['solrsort'] : '';

  try {
    $results = apachesolr_search_run($keys, $filters, $solrsort, $page->path, pager_find_page());
    return $results;
  }
  catch (Exception $e) {
    watchdog('Apache Solr', nl2br(check_plain($e->getMessage())), NULL, WATCHDOG_ERROR);
    apachesolr_failure(t('Solr search'), $keys);
  }
  
  return $results;
}

/**
 * Function for preprocessing the variables for the apachesolr_page_results
 * theme.
 *
 * @param array $variables
 *   An associative array containing:
 *   - index: The index this search was executed on.
 *   - results: An array of search results, as returned by
 *     SearchApiQueryInterface::execute().
 *   - keys: The keywords of the executed search.
 */
function template_preprocess_apachesolr_page_results(array &$variables) {
  /*if (!empty($variables['results']['results'])) {
    $variables['entities'] = entity_load($variables['index']->entity_type, array_keys($variables['results']['results']));
  }*/
}

/**
 * Theme function for displaying search results.
 *
 * @param array $variables
 *   An associative array containing:
 *   - index: The index this search was executed on.
 *   - results: An array of search results, as returned by
 *     SearchApiQueryInterface::execute().
 *   - entities: The loaded entities for all results, in an array keyed by ID.
 *   - "view_mode": The view mode to use for displaying the individual results,
 *     or the special mode "apachesolr_page_result" to use the theme function
 *     of the same name.
 *   - keys: The keywords of the executed search.
 */
function theme_apachesolr_page_results(array $variables) {
  drupal_add_css(drupal_get_path('module', 'apachesolr_page') . '/apachesolr_page.css');

  $results = $variables['results'];
  $keys = $variables['keys'];

  $output = '';
  
  if (empty($results)) {
    $output .= "\n<h2>" . t('Your search yielded no results') . "</h2>\n";
    return $output;
  }

  $output .= "\n<h2>" . t('Search results') . "</h2>\n";

  if ($variables['view_mode'] == 'apachesolr_page_result') {
    $output .= '<ol class="search-results">';
    foreach ($results as $item) {
      $output .= '<li class="search-result">' . theme('apachesolr_page_result', array('result' => $item)) . '</li>';
    }
    $output .= '</ol>';
  }
  else {
    $output .= '<div class="search-results">';
    //$output .= render(entity_view($index->entity_type, $entities, $variables['view_mode']));
    $output .= '</div>';
  }

  return $output;
}

/**
 * Theme function for displaying search results.
 *
 * @param array $variables
 *   An associative array containing:
 *   - index: The index this search was executed on.
 *   - result: One item of the search results, an array containing the keys
 *     'id' and 'score'.
 *   - entity: The loaded entity corresponding to the result.
 *   - keys: The keywords of the executed search.
 */
function theme_apachesolr_page_result(array $variables) {
  $id = $variables['result']['fields']['id'];
  $entity = $variables['result']['entity_type'];

  $url = $variables['result']['link'];
  $name = $variables['result']['title'];

  if (!empty($variables['result']['snippets'][0])) {
    $text = $variables['result']['snippets'][0];
  }
  else {
/*    $fields = $variables['result']['fields'];
    $fields = array_intersect_key($fields, drupal_map_assoc($index->getFulltextFields()));
    $fields = apachesolr_extract_fields($wrapper, $fields);
    $text = '';
    $length = 0;
    foreach ($fields as $field) {
      if (apachesolr_is_list_type($field['type'])) {
        continue;
      }
      $val_length = drupal_strlen($field['value']);
      if ($val_length > $length) {
        $text = $field['value'];
        $length = $val_length;
      }
    }
    if ($text && function_exists('text_summary')) {
      $text = text_summary($text);
    }
*/
  }

  $output = '<h3>' . ($url ? l($name, $url) : check_plain($name)) . "</h3>\n";
  if ($text) {
    $output .= $text;
  }

  return $output;
}
