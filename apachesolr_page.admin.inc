<?php

/**
 * Displays an overview of all defined search pages.
 */
function apachesolr_page_admin_overview() {
  $base_path = drupal_get_path('module', 'apachesolr_page') . '/';
  drupal_add_css($base_path . 'apachesolr_page.admin.css');

  $header = array(t('Status'), t('Name'), t('Path'), t('Operations'));

  $rows = array();
  $t_enabled['data'] = array(
    '#theme' => 'image',
    '#path' => $base_path . 'enabled.png',
    '#alt' => t('enabled'),
    '#title' => t('enabled'),
  );
  $t_enabled['class'] = array('apachesolr-status');
  $t_disabled['data'] = array(
    '#theme' => 'image',
    '#path' => $base_path . 'disabled.png',
    '#alt' => t('disabled'),
    '#title' => t('disabled'),
  );
  $t_disabled['class'] = array('apachesolr-status');
  $t_enable = t('enable');
  $t_disable = t('disable');
  $t_edit = t('edit');
  $t_delete = t('delete');
  $pre = 'admin/config/search/apachesolr/page/';
  $pre_index = 'admin/config/search/apachesolr/';
  $enable = '/enable';
  $disable = '/disable';
  $edit = '/edit';
  $delete = '/delete';

  //$indexes = apachesolr_index_load_multiple(FALSE);
  foreach (apachesolr_page_load_multiple() as $page) {
    $url = $pre . $page->id;
    //$index = $indexes[$page->index_id];
    $rows[] = array(
      $page->enabled ? $t_enabled : $t_disabled,
      l($page->name, $page->path),
      l($page->path, $page->path),
//      l($index->name, $index->machine_name),
      l($t_edit, $url . $edit),
    );
  }

  return array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('There are no search pages defined yet.'),
  );
}

/**
 * Displays a form for adding a search page.
 */
function apachesolr_page_admin_add(array $form, array &$form_state) {
  $form = array();
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Search name'),
    '#maxlength' => 50,
    '#required' => TRUE,
  );
  $form['filters'] = array(
    '#type' => 'textfield',
    '#title' => t('filters'),
    '#description' => t('Default facets to be applied to this page.'),
    '#required' => TRUE,
    '#field_prefix' => '?filters=',
  );
  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Search description'),
  );
  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#description' => t('Set the path under which the search page will be accessible, when enabled.'),
    '#maxlength' => 50,
    '#required' => TRUE,
  );

    $modes = array();
    $modes['direct'] = t('Direct query');
    $modes['single'] = t('Single term');
    $modes['terms'] = t('Multiple terms');
  
  $form['options']['#tree'] = TRUE;
  $form['options']['mode'] = array(
    '#type' => 'select',
    '#title' => t('Query type'),
    '#description' => t('Select how the query will be parsed.'),
    '#options' => $modes,
    '#default_value' => 'terms',
  );

  $form['options']['per_page'] = array(
    '#type' => 'select',
    '#title' => t('Results per page'),
    '#description' => t('Select how many items will be displayed on one page of the search result.'),
    '#options' => drupal_map_assoc(array(5, 10, 20, 30, 40, 50, 60, 80, 100)),
    '#default_value' => 10,
  );

/*  $entity_info = entity_get_info($index->entity_type);
  $view_modes = array(
    'apachesolr_page_result' => t('Themed as search results'),
  );
  foreach ($entity_info['view modes'] as $mode => $mode_info) {
    $view_modes[$mode] = $mode_info['label'];
  }
  if (count($view_modes) > 1) {
    $form['view_mode'] = array(
      '#type' => 'select',
      '#title' => t('View mode'),
      '#options' => $view_modes,
      '#description' => t('Select how search results will be displayed.'),
      '#size' => 1,
      '#default_value' => 'apachesolr_page_result',
    );
  }
  else {
    $form['view_mode'] = array(
      '#type' => 'value',
      '#value' => reset($view_modes),
    );
  }*/

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create page'),
  );

  return $form;
}

/**
 * Validation callback for apachesolr_page_admin_add().
 */
function apachesolr_page_admin_add_validate(array $form, array &$form_state) {
  $form_state['values']['path'] = drupal_strtolower(trim($form_state['values']['path']));
  if (apachesolr_page_load_multiple(FALSE, array('path' => $form_state['values']['path']))) {
    form_set_error('path', t('The entered path is already in use. Please enter a unique path.'));
  }
}

/**
 * Submit callback for apachesolr_page_admin_add().
 */
function apachesolr_page_admin_add_submit(array $form, array &$form_state) {
  form_state_values_clean($form_state);
  $values = $form_state['values'];
  apachesolr_page_insert($values);
  drupal_set_message(t('The search page was successfully created.'));
  $form_state['redirect'] = 'admin/config/search/apachesolr/page';
}

/**
 * Displays a form for editing or deleting a search page.
 */
function apachesolr_page_admin_edit(array $form, array &$form_state, stdClass $page) {
  $form_state['page'] = $page;

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Search name'),
    '#maxlength' => 50,
    '#required' => TRUE,
    '#default_value' => $page->name,
  );
  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#description' => t('This will only take effect if the selected index is also enabled.'),
    '#default_value' => $page->enabled,
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Search description'),
    '#default_value' => $page->description,
  );
  $form['filters'] = array(
    '#type' => 'textfield',
    '#title' => t('Filters'),
    '#default_value' => $page->filters,
    '#description' => t('Default facets to be applied to this page.'),
    '#required' => TRUE,
    '#fiueld_prefix' => '?filters=',
  );
  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#description' => t('Set the path under which the search page will be accessible, when enabled.'),
    '#maxlength' => 50,
    '#default_value' => $page->path,
  );

  $modes = array();
  $modes['direct'] = t('Direct query');
  $modes['single'] = t('Single term');
  $modes['terms'] = t('Multiple terms');
  
  $form['options']['#tree'] = TRUE;
  $form['options']['mode'] = array(
    '#type' => 'radios',
    '#title' => t('Query type'),
    '#description' => t('Select how the query will be parsed.'),
    '#options' => $modes,
    '#default_value' => $page->options['mode'],
  );

/*  $fields = array();
  if (!empty($index->options['fields'])) {
    foreach ($index->getFulltextFields(TRUE) as $name) {
      $fields[$name] = $index->options['fields'][$name]['name'];
    }
  }
  if (count($fields) > 1) {
    $form['options']['fields'] = array(
      '#type' => 'select',
      '#title' => t('Searched fields'),
      '#description' => t('Select the fields that will be searched. If no fields are selected, all available fulltext fields will be searched.'),
      '#options' => $fields,
      '#size' => min(4, count($fields)),
      '#multiple' => TRUE,
      '#default_value' => $page->options['fields'],
    );
  }
  else {
    $form['options']['fields'] = array(
      '#type' => 'value',
      '#value' => array(),
    );
  }*/

  $form['options']['per_page'] = array(
    '#type' => 'select',
    '#title' => t('Results per page'),
    '#description' => t('Select how many items will be displayed on one page of the search result.'),
    '#options' => drupal_map_assoc(array(5, 10, 20, 30, 40, 50, 60, 80, 100)),
    '#default_value' => $page->options['per_page'],
  );

/*  $entity_info = entity_get_info($index->entity_type);
  $view_modes = array(
    'apachesolr_page_result' => t('Themed as search results'),
  );
  foreach ($entity_info['view modes'] as $mode => $mode_info) {
    $view_modes[$mode] = $mode_info['label'];
  }
  if (count($view_modes) > 1) {
    $form['options']['view_mode'] = array(
      '#type' => 'select',
      '#title' => t('View mode'),
      '#options' => $view_modes,
      '#description' => t('Select how search results will be displayed.'),
      '#size' => 1,
      '#default_value' => isset($page->options['view_mode']) ? $page->options['view_mode'] : 'apachesolr_page_result',
    );
  }
  else {
    $form['options']['view_mode'] = array(
      '#type' => 'value',
      '#value' => reset($view_modes),
    );
  }*/

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );
  $form['actions']['delete'] = array(
    '#type' => 'fieldset',
    '#title' => t('Delete search page'),
    '#description' => t('This will delete the search page along with all of its settings.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    'delete' => array(
      '#type' => 'submit',
      '#value' => t('Delete search page'),
    ),
  );

  return $form;
}

/**
 * Validation callback for apachesolr_page_admin_edit().
 */
function apachesolr_page_admin_edit_validate(array $form, array &$form_state) {
  if ($form_state['values']['op'] == t('Save changes')) {
    $form_state['values']['path'] = drupal_strtolower(trim($form_state['values']['path']));
    $pages = apachesolr_page_load_multiple(FALSE, array('path' => $form_state['values']['path']));
    if (count($pages) > 1 || (($page = array_shift($pages)) && $page->id != $form_state['page']->id)) {
      form_set_error('path', t('The entered path is already in use. Please enter a unique path.'));
    }
  }
}

/**
 * Submit callback for apachesolr_page_admin_edit().
 */
function apachesolr_page_admin_edit_submit(array $form, array &$form_state) {
  $op = $form_state['values']['op'];
  form_state_values_clean($form_state);
  $form_state['redirect'] = 'admin/config/search/apachesolr/page';

  if ($op == t('Delete search page')) {
    apachesolr_page_delete($form_state['page']->id);
    drupal_set_message(t('The search page was successfully deleted.'));
    return;
  }
  apachesolr_page_edit($form_state['page']->id, $form_state['values']);
  drupal_set_message(t('The changes were successfully saved.'));
}
